# agenda de contatos


## Name
Sistema de agendas em python 3 console

## Description
Com intuito de estudo criado um sistema de agenda onde o usuario pode criar contatos, consultar e remover.

## Installation
Por não ter libs ainda o processo básico: ter python na ultima versao e rodar o script qualquer duvida abre um issue

## Usage
Para usar execute o script e escolha as opções

## Support
Para ajudar outros iniciantes ou avançados hahah pode chamar no whatsapp > +5585986455855 ou email: integrationsdeveloper39@gmail.com

## Roadmap
Para criar esse software é preciso conhecer: for, while, condição if, elif e else, listas

## Contributing
Caso queira contribuir pode abrir um pr/issue e irei avaliar

## License
projeto open source

## Project status
proximas atualizações serão validar variaveis e mais validações 
