"""Crie uma agenda de contatos onde o usuário pode
adicionar novos contatos,
remover contatos existentes e
buscar por contatos.
Adicione informações como
número de telefone, e-mail e endereço."""

agenda = [[],[],[]]

def adicionar(agenda):
    agenda[0].append(int(input("Telefone:")))
    agenda[1].append(str(input("E-mail:")))
    agenda[2].append(str(input("Endereço:")))

def buscar(agenda):
    for id, dado in enumerate(agenda[0]):
        print(f"Id: {id}.......{dado}")
    print("\n")
    contato = int(input("Digite apenas o id\nQual contato deseja consultar?\n"))
    print(f"Telefone:{agenda[0][contato]}")
    print(f"E-mail:{agenda[1][contato]}")
    print(f"Endereço:{agenda[2][contato]}")

def remover(agenda):
    for id, dado in enumerate(agenda[0]):
        print(f"Id: {id}.......{dado}")
    print("\n")
    contato = int(input("Digite apenas o id\nQual contato deseja remover?\n"))
    agenda[0].pop(contato)
    agenda[1].pop(contato)
    agenda[2].pop(contato)
    print(agenda)

while True:
    op = int(input("[1]Criar Contato\n[2]Consultar Contatos\n[3]Remover Contatos\n[4]Sair\nDigite a opcao:"))

    if op == 1:
        adicionar(agenda)
    elif op == 2:
        buscar(agenda)
    elif op == 3:
        remover(agenda)
    else:
        break